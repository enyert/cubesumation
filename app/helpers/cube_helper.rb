module CubeHelper
    class Coordinate
        def initialize(x, y, z)
            @x = x - 1
            @y = y - 1
            @z = z - 1
        end
    
        def transform1D(n)
            return @x + n * @y + n * n * @z  
        end
    end
    
    class Query
        def initialize(type, coordinates, w)
            @type = type
            @coordinates = coordinates  
            @w = w
        end
    
        def getType()
      	    return @type
        end
    
        def getCoordinates()
      	    return @coordinates
        end
    
        def getW()
      	    return @w
        end
    end
    
    class TestCase
        def initialize(query_list, n)
            @query_list = query_list
            @n = n
        end 
    
        def updateQuery(value_list, i)
            value_list[@query_list[i].getCoordinates[0].transform1D(@n)] = @query_list[i].getW 
        end
    
        def sumQuery(value_list, i)
            sum = 0
            sub_list = []
            min_value = @query_list[i].getCoordinates[0].transform1D(@n)
            max_value = @query_list[i].getCoordinates[1].transform1D(@n)
            aux = 0
    
            if min_value > max_value
                aux = min_value
                min_value = max_value
                max_value = aux
            end
    
            cant = max_value - min_value + 1
            sub_list = value_list[min_value, cant]
            sub_list.each { |item|  sum += item}
            return sum
        end
      
        def makeQueries(value_list)
            result = ""
            i = 0
            while i < @query_list.length do
                if @query_list[i].getType.eql?("UPDATE")
                    updateQuery(value_list, i)
                end
          
                if @query_list[i].getType.eql?("QUERY")
                    result = result + sumQuery(value_list, i).to_s + "\n"
                end
          
                i += 1
            end
            return result
        end
    end
    
    class MatrixReader
        def initialize(entry)
      	    @entry = entry
        end
    
        def get_n(line)
      	    return line.split(" ")[0].to_i
        end
    
        def get_m(line)
      	    return line.split(" ")[1].to_i
        end
    
        def read_query(line)
      	    query = nil
      	    coordinates = []
      	    tokens = line.split(" ")
      	    if tokens[0].eql?("UPDATE")
      	        c1 = Coordinate.new(tokens[1].to_i, tokens[2].to_i, tokens[3].to_i)	
      	        w = tokens[4].to_i
      	        query = Query.new("UPDATE", [c1], w)
      	    end
    
      	    if tokens[0].eql?("QUERY")
      	        c1 = Coordinate.new(tokens[1].to_i, tokens[2].to_i, tokens[3].to_i)
      	        c2 = Coordinate.new(tokens[4].to_i, tokens[5].to_i, tokens[6].to_i)
      	        w = 0
      	        query = Query.new("QUERY", [c1, c2], w)
      	    end
      	    return query
        end
    
        def read_entry
      	    result = ""
      	    query_list = []
      	    lines = @entry.split("\n")
      	    t = lines[0].to_i
      	    i = 0
      	    k = 0
      	    l = 1
      	    while i < t do
      	        query_list.clear
      	        n = get_n(lines[l])
      	        m = get_m(lines[l])
      	        k = 0
      	        l += 1
      	        #reading queries
      	        while k < m do
      	  	        query_list.push(read_query(lines[l + k]))
      	  	        k += 1
      	        end
      	        vl = Array.new(n * n * n, 0) #Create a flat 1D Array
      	        tc = TestCase.new(query_list, n)
      	        result = result + tc.makeQueries(vl)
      	        l += m 
      	        i += 1
      	    end
      	    return result 
        end
    end

end
